﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BrightIdeasSoftware
{
    public class ExScrollEventArgs : ScrollEventArgs
    {
        private int _npage;

        public int NPageValue
        {
            get
            {
                return this._npage;
            }
            set
            {
                this._npage = value;
            }
        }

        public ExScrollEventArgs()
            : base(ScrollEventType.EndScroll, 0)
        {
        }

        public ExScrollEventArgs(ScrollEventType scrollEventType, int oldVal, int newVal, ScrollOrientation verticalScroll, int nPage = 0)
            : base(scrollEventType, oldVal, newVal, verticalScroll)
        {
            this.NPageValue = nPage;
        }
    }
}
